#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom

scores = ["buenisimo", "bueno", "regular", "malo", "malisimo"]

def read_joke(node):
    """Given a chiste DOM node, get its score and contents

    Returns a dictionary with 'score', 'question' and 'answer' as keys.
    """
    score = node.getAttribute('calificacion')
    questions = node.getElementsByTagName('pregunta')
    question = questions[0].firstChild.nodeValue.strip()
    answers = node.getElementsByTagName('respuesta')
    answer = answers[0].firstChild.nodeValue.strip()
    contents = {'score': score, 'question': question, 'answer': answer}
    return contents

def main():
    """Programa principal"""
    document = xml.dom.minidom.parse("chistes.xml")
    jokes = document.getElementsByTagName('chiste')

    for joke in reversed(jokes):
        texts = read_joke(joke)
        print(f"Calificación: {texts['score']}.")
        print(f" Respuesta: {texts['answer']}")
        print(f" Pregunta: {texts['question']}")

if __name__ == "__main__":
    main()
