#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import sys

import smil

class SMILJSON(smil.SMIL):

    def json(self):

        els = []
        for el in self.elements():
            dict_el = {
                'name': el.name(),
                'attrs': el.attrs()
            }
            els.append(dict_el)
        return json.dumps(els, sort_keys=True, indent=2)

def main(path):
    """Programa principal"""

    karaoke = SMILJSON(path)
    karaoke_json = karaoke.json()
    print(karaoke_json)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit("Usage: python3 json.py <file>")
    main(sys.argv[1])

