#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom

class Element():

    def __init__(self, name, attrs):

        self._name = name
        self._attrs = attrs

    def name(self):

        return self._name

    def attrs(self):

        return self._attrs


class SMIL:

    def __init__(self, path):

        self.dom = xml.dom.minidom.parse(path)

    @classmethod
    def _child_els(cls, node):

        if node.nodeType != node.ELEMENT_NODE:
            return []
        attrs = {}
        for attr in node.attributes.items():
            attrs[attr[0]] = attr[1]
        els = [ Element(name=node.tagName, attrs=attrs) ]
        for child in node.childNodes:
            els.extend(cls._child_els(child))
        return els

    def elements(self):

        root = self.dom.documentElement
        els = self._child_els(root)
        return els
