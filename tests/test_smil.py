#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import unittest

import smil

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

karaoke_names = ['smil', 'head', 'layout', 'root-layout', 'region',
                 'region', 'region', 'body', 'par', 'img', 'img',
                 'audio', 'textstream', 'audio']

karaoke_attrs = [{},
 {},
 {},
 {'background-color': 'blue', 'height': '300', 'width': '248'},
 {'id': 'a', 'left': '64', 'top': '20'},
 {'id': 'b', 'left': '20', 'top': '120'},
 {'id': 'text_area', 'left': '20', 'top': '100'},
 {},
 {},
 {'begin': '2s',
  'dur': '36s',
  'region': 'a',
  'src': 'http://www.content-networking.com/smil/hello.jpg'},
 {'begin': '12s',
  'end': '48s',
  'region': 'b',
  'src': 'http://www.content-networking.com/smil/earthrise.jpg'},
 {'begin': '1s', 'src': 'http://www.content-networking.com/smil/hello.wav'},
 {'fill': 'freeze',
  'region': 'text_area',
  'src': 'http://gsyc.es/~grex/letra.rt'},
 {'begin': '4s', 'src': 'cancion.ogg'}
]

class TestElement(unittest.TestCase):

    def test_simple(self):

        el = smil.Element(name='el', attrs={'name1': 'value1', 'name2': 'value2'})
        self.assertEqual('el', el.name())
        self.assertEqual({'name1': 'value1', 'name2': 'value2'}, el.attrs())


class TestSmil(unittest.TestCase):

    def setUp(self):
        os.chdir(parent_dir)

    def test_children(self):

        smil_object = smil.SMIL("karaoke.smil")
        els = smil_object.elements()
        names = [el.name() for el in els]
        self.assertEqual(karaoke_names, names)

    def test_children_Attrs(self):

        smil_object = smil.SMIL("karaoke.smil")
        els = smil_object.elements()
        attrs = [el.attrs() for el in els]
        self.assertEqual(karaoke_attrs, attrs)


if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
